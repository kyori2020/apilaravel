<?php

namespace App\Http\Controllers;
use App\Models\Libro;

use Illuminate\Http\Request;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        //
        $query = Libro::query();

        // Filtrar por categoría
        if ($request->has('categoria')) {
            $categoria = $request->input('categoria');
            if (!empty($categoria)) {
                $query->whereHas('categoria', function ($categoriaQuery) use ($categoria) {
                    $categoriaQuery->where('nombre', $categoria);
                });
            }
        }
    
        // Filtrar por precio
        if ($request->has('precio')) {
            $precio = $request->input('precio');
        
            switch ($precio) {
                case 0:
                    // Todos los precios mayores a 0
                    $query->where('precio', '>', 0);
                    break;
                case 1:
                    // Precios mayores a 0 y menores a 20
                    $query->whereBetween('precio', [0, 20]);
                    break;
                case 2:
                    // Precios mayores a 20 y menores a 50
                    $query->whereBetween('precio', [21, 50]);
                    break;
                case 3:
                    // Precios entre 50 y 100
                    $query->whereBetween('precio', [51, 100]);
                    break;
                case 4:
                    // Precios mayores a 100
                    $query->where('precio', '>', 100);
                    break;
            }
        }
    
        // Filtrar por nombre de libro
        if ($request->has('titulo')) {
            $query->where('titulo', 'like', '%' . $request->input('titulo') . '%');
        }
    
        $libros = $query->with('categoria')->get();
    
        return response()->json([
            'success' => true,
            'data' => $libros,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'titulo' => 'required',
            'isbn' => 'required',
            'numero_paginas' => 'required',
            'categoria_id' => 'required|exists:categorias,id',
            'imagen' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $libro = new Libro();
        $libro->titulo = $request->titulo;
        $libro->isbn = $request->isbn;
        $libro->fecha = $request->fecha;
        $libro->precio = $request->precio;
        $libro->numero_paginas = $request->numero_paginas;
        $libro->categoria_id = $request->categoria_id;

        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen');
            $nombreImagen = time() . '_' . $imagen->getClientOriginalName();
            $rutaImagen = $imagen->storeAs('imagenes', $nombreImagen, 'public');
            $libro->imagen_url = $rutaImagen;
        }

        $libro->save();

        return response()->json([
            'success' => true,
            'data' => $libro,
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $libro->load('categoria');

        return response()->json([
            'success' => true,
            'data' => $libro,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Libro $libro)
    {
        //
   


    $request->validate([
        'titulo' => 'required',
        'isbn' => 'required',
        'numero_paginas' => 'required',
        'categoria_id' => 'required|exists:categorias,id',
        'imagen' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
    ]);
        $libro->titulo = $request->titulo;
        $libro->isbn = $request->isbn;
        $libro->fecha = $request->fecha;
        $libro->precio = $request->precio;
        $libro->numero_paginas = $request->numero_paginas;
        $libro->categoria_id = $request->categoria_id;

        if ($request->hasFile('imagen')) {
            $imagen = $request->file('imagen');
            $nombreImagen = time() . '_' . $imagen->getClientOriginalName();
            $rutaImagen = $imagen->storeAs('imagenes', $nombreImagen, 'public');
            $libro->imagen_url = $rutaImagen;
        }

        $libro->save();
    
        return response()->json([
            'success' => true,
            'data' => $libro,
        ], 200);
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Libro $libro)
    {
        //
        $libro->delete();
        return response()->json([
            'success' => true,
            'message' => 'El libro se ha eliminado correctamente.',
        ]);
    }
}
